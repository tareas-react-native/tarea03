import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Image, Linking, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { baseUri } from '../../rowData'

const styles = StyleSheet.create({
    container: {
        backgroundColor: "#bdc3c7",
        borderRadius: 7,
        marginBottom: 10,
    },
    imageContainer:{
        height:100,
    },
    image:{
        borderTopLeftRadius: 7,
        borderTopRightRadius: 7,
        backgroundColor: "black",
        width: "100%",
        height: "100%"
    },
    textContainer:{
        padding: 10
    },
    title: {
        fontWeight: "bold",
        fontSize: 15,
    },
    information:{
        flexDirection: "row",
        justifyContent: "space-between",
        marginTop: 10,
        alignItems: "center"
    },
    link:{
        padding: 10,
        color: "#3498db",
        textDecorationLine: "underline" 
    },
    buttonContainer:{
        backgroundColor: "#8e44ad",
        borderBottomLeftRadius: 7,
        borderBottomRightRadius: 7,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonText:{
        color:"#ecf0f1",
        fontSize: 20,
    } 
})


const RenderBoton = ({mostrar, addDishes, title}) => {

    console.log("mostrar ", mostrar)
    if (mostrar){
        return (
            <TouchableOpacity style={styles.buttonContainer} onPress={() => addDishes(title)}>
                <Text style={styles.buttonText}>
                    Agregar al carro
                </Text>
            </TouchableOpacity>
        )
    }
    return null
}


const OpenUrl = ({sourceUrl}) => {
    
    console.log("sourceUrl ",sourceUrl);
    return (
        <TouchableOpacity
            onPress={() => Linking.openURL(sourceUrl)}
        >
            <Text style={styles.link}>Ver Receta</Text>
        </TouchableOpacity>
   )
}

/*
    
const OpenUrl = (sourceUrl) => {
    
    console.log("sourceUrl ",sourceUrl);
    return (
        <TouchableOpacity
            onPress={() => Linking.openURL(sourceUrl.sourceUrl)}
        >
            <Text>Ver Receta</Text>
        </TouchableOpacity>
   )
} 

<OpenUrl sourceUrl={sourceUrl}></OpenUrl>
*/


const DishCard = ({title, readyInMinutes, servings, image, addDishes, mostrarBotonAgregar}) => (
    <View style={styles.container}>
        <View style={styles.imageContainer}>
            <Image style={styles.image}
                resizeMode="cover"
                source={{uri: `${baseUri}${image}`}}/>
        </View>
        <View style={styles.textContainer}>
            <Text style={styles.title}>
                {title}
            </Text>
            <View style={styles.information}>
                <Text >{`Listo en ${readyInMinutes} min.`}</Text>
                <Text >{`Para ${servings} personas`}</Text>
            </View>
        </View>        

        <RenderBoton mostrar={mostrarBotonAgregar} addDishes={addDishes} title={title}/>
    </View>
);

export default DishCard;

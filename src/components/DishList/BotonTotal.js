import React, { Component } from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'

const styles = StyleSheet.create({
    container: {
        backgroundColor: "#49a35a",
        borderRadius: 10,
        height: 50,
        alignItems: "center",
        margin: 20,
        flexDirection: "row",
        justifyContent: "space-between",
        padding: 20,
        height: 70
    },
    containerHijo: {
        backgroundColor: "#b93d30",
        borderRadius: 10,
        height: 50,
        alignItems: "center",
        justifyContent: "center",
        padding: 20,
        height: 50
    },
    textButton:{
        color:"#ecf0f1",
        fontSize: 20,
    }
})


const BotonTotal = ({ total, emptyCartDishes }) =>(
    <TouchableOpacity style={styles.container}>
        <Text style={styles.textButton}>
            Total Carrito ({total})
        </Text>
        <TouchableOpacity style={styles.containerHijo} onPress={() => emptyCartDishes()}>
            <Text style={styles.textButton} >
                Vaciar Carrito
            </Text>
        </TouchableOpacity>
    </TouchableOpacity>
);


export default BotonTotal

/*
export default class BotonTotal extends Component {

    render() {
        const { total, emptyCartDishes } = this.props
        
        return (
            <TouchableOpacity style={styles.container}>
                <Text style={styles.textButton}>
                    Total Carrito ({total})
                </Text>
                <TouchableOpacity style={styles.containerHijo} onPress={() => emptyCartDishes()}>
                    <Text style={styles.textButton} >
                        Vaciar Carrito
                    </Text>
                </TouchableOpacity>
            </TouchableOpacity>
        )
    }
}

*/
